from flask import Flask, Blueprint, jsonify, request

api_blueprint = Blueprint('api', __name__)

measures = [
    {"id": 1, "timestamp": "09/04/2024, 11:00:00", "temp": "21.5"},
    {"id": 2, "timestamp": "09/04/2024, 12:00:00", "temp": "21.1"},
]

display_measures = [
    {"id": 2, "timestamp": "09/04/2024, 12:00:00", "temp": "21.1"},
]

IDcounter = 2

def get_measure_id():
    global IDcounter
    IDcounter += 1
    return IDcounter


@api_blueprint.route('/api/measures/<int:measure_id>', methods=['GET'])
def get_measure(measure_id):
    measure = next((measure for measure in measures if measure['id'] == measure_id), None)
    if measure:
        return jsonify(measure)
    else:
        return jsonify({"message": "measure not found"}), 404


@api_blueprint.route('/api/measures', methods=['POST'])
def create_measure():
    new_measure = request.json
    new_measure['id'] = get_measure_id()
    measures.append(new_measure)
    print(measures)
    return jsonify(new_measure), 201

@api_blueprint.route('/api/measures/<int:measure_id>', methods=['PUT'])
def update_measure(measure_id):
    measure = next((measure for measure in measures if measure['id'] == measure_id), None)
    if measure:
        measure.update(request.json)
        print(measures)
        return jsonify(measure)
    else:
        return jsonify({"message": "measure not found"}), 404

@api_blueprint.route('/api/measures/<int:measure_id>', methods=['DELETE'])
def delete_measure(measure_id):
    global measures
    measures = [measure for measure in measures if measure['id'] != measure_id]
    print(measures)
    return jsonify({"message": "measure deleted"}), 200

@api_blueprint.route('/api/range/<int:range>', methods=['DELETE'])
def delete_range(range):
    global measures
    if (range > 0 and range <= len(measures)):
        measures = measures[range:]
        print(measures)
        return jsonify({"message": f"Deleted first {range} measurements"}), 200
    else:
        return jsonify({"error": "Invalid delete count"}), 400

@api_blueprint.route('/api/range/<int:range>', methods=['GET'])
def display_range(range):
    global measures
    global display_measures
    if (range > 0 and range <= len(measures)):
        display_measures = []
        display_measures = measures[-range:]
        print(display_measures)
        return jsonify({"message": f"Displaying last {range} measurements"}), 200
    else:
        return jsonify({"error": "Invalid display count"}), 400

@api_blueprint.route('/api/measures', methods=['GET'])
def get_measures():
    return jsonify(measures)