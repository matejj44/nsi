from flask import Flask, jsonify, request, render_template
from api_routes import api_blueprint, measures, display_measures

app = Flask(__name__)

app.register_blueprint(api_blueprint)

#DIFF APP

@app.route('/')
def index():
    return render_template('base.html')

@app.route('/login')
def login():
    return render_template("login.html")

@app.route('/register')
def register():
    return render_template("register.html")

@app.route('/<name>')
def dashboard(name=""):
    return render_template("dashboard.html", name = name, measures = measures, display_measures=display_measures)

if __name__ == "__main__":
    app.run(host='127.0.0.1', port=5000, debug=True, use_reloader = False)